## Description

NestJs framework with MikroORM coding challenge.

## Installation

```bash
Edit src/mikro-orm.config.ts or add an .env file with your MySql database credentials

$ npm install
$ mikro-orm schema:create --run
```

## Running the app

```bash
$ npm run start
```

## Test

```bash
$ npm run test:e2e
```

This will run two tests:

- Simple server ping
- Create user race condition test

## Remarks

There are comments in the code to explain the reasoning behind certain decisions.

Potential improvements:

- Add Unit of Work in order to simplify transaction management
- Split services into application and domain services to separate user and invite code logic assuming there is much more
  business logic to be added
- Technically the entities have hard dependencies to the MikroOrm library because of the decorators and should usually
  be defined in an infrastructure layer, e.g. via schema definitions
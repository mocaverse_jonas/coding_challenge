import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { UserModule } from './user/user.module';
import config from './mikro-orm.config';

@Module({
  imports: [MikroOrmModule.forRoot(config), UserModule],
  controllers: [AppController],
})
export class AppModule {}

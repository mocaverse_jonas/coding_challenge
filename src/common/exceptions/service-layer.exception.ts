import { BadRequestException } from '@nestjs/common';

export class ServiceLayerException extends BadRequestException {
  constructor(error: string) {
    super(error);
  }
}

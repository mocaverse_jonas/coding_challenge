import { customAlphabet } from 'nanoid';
import { ClassConstructor } from 'class-transformer/types/interfaces';
import { plainToInstance } from 'class-transformer';

// removed 0, 1, I, O and only uppercase to avoid confusion
const alphabet = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
export const generateUserFriendlyId = customAlphabet(alphabet, 10);

export const map = <T, V>(params: { from: T; to: ClassConstructor<V> }) => {
  return plainToInstance(params.to, params.from, {
    excludeExtraneousValues: true,
  });
};

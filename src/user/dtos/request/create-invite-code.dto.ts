import { IsPositive } from 'class-validator';

export class CreateInviteCodeDto {
  @IsPositive()
  maxUses: number;
}

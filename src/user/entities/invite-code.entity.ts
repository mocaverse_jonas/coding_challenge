import { Entity, ManyToOne, Property } from '@mikro-orm/core';
import { BaseEntity } from '../../common/entities/base.entity';
import { User } from './user.entity';

@Entity()
export class InviteCode extends BaseEntity {
  @Property({ unique: true, index: true })
  code!: string;

  @Property({ default: 1 })
  maxUses!: number;

  @Property({ default: 0 })
  used!: number;

  @ManyToOne(() => User)
  referrer!: User;

  constructor(code: string, maxUses: number, referrer: User) {
    super();
    this.code = code;
    this.maxUses = maxUses;
    this.referrer = referrer;
  }
}

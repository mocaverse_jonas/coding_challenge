import { Entity, ManyToOne, Opt, Property } from '@mikro-orm/core';
import { BaseEntity } from '../../common/entities/base.entity';
import { InviteCode } from './invite-code.entity';

@Entity()
export class User extends BaseEntity {
  @Property({ unique: true })
  email!: string;
  // Added the invite code relationship as many-to-one here, because users might be
  // able to change their email addresses later on, and we probably want to keep
  // track of the actual user who used the code (given users can only be "soft" deleted)
  @ManyToOne(() => InviteCode, { nullable: true })
  inviteCode: Opt<InviteCode>; // Optional since a user might not be created via invite code

  constructor(email: string, inviteCode?: InviteCode) {
    super();
    this.email = email;
    this.inviteCode = inviteCode;
  }
}

import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dtos/request/create-user.dto';
import { CreateInviteCodeDto } from './dtos/request/create-invite-code.dto';

@Controller()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('user')
  create(@Body() dto: CreateUserDto) {
    return this.userService.create(dto);
  }

  // this endpoint needs to enforce user authentication
  @Post('code')
  async createInviteCode(@Body() dto: CreateInviteCodeDto) {
    const inviteCode = await this.userService.createInviteCode(dto);
    return { inviteCode };
  }

  @Get('verifyCode')
  async isInviteCodeValid(@Query('inviteCode') code: string) {
    const isValid = await this.userService.isInviteCodeValid(code);
    return { isValid };
  }

  @Get('isEmailUsed')
  async userExists(@Query('email') email: string) {
    const isUsed = await this.userService.userExists(email);
    return { isUsed };
  }
}

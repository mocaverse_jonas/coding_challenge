import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { User } from './entities/user.entity';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { InviteCode } from './entities/invite-code.entity';

@Module({
  imports: [MikroOrmModule.forFeature([User, InviteCode])],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}

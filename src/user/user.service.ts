import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dtos/request/create-user.dto';
import { InviteCode } from './entities/invite-code.entity';
import { InjectRepository } from '@mikro-orm/nestjs';
import { User } from './entities/user.entity';
import { EntityRepository, LockMode } from '@mikro-orm/core';
import { CreateInviteCodeDto } from './dtos/request/create-invite-code.dto';
import { generateUserFriendlyId, map } from '../common/utils';
import { ServiceLayerException } from '../common/exceptions/service-layer.exception';
import { UserResponseDto } from './dtos/response/user.response.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: EntityRepository<User>,
    @InjectRepository(InviteCode)
    private inviteCodeRepository: EntityRepository<InviteCode>,
  ) {}

  async create(dto: CreateUserDto) {
    const em = this.userRepository.getEntityManager();

    const user = await em.transactional(async (em) => {
      // some databases do not support pessimistic locking, in that case we would
      // need to use optimistic locking e.g. on the "used" field.
      const inviteCode = await this.inviteCodeRepository.findOne(
        {
          code: dto.inviteCode,
        },
        { lockMode: LockMode.PESSIMISTIC_WRITE },
      );

      if (!inviteCode) {
        throw new ServiceLayerException('Invalid invite code');
      }
      if (inviteCode.used >= inviteCode.maxUses) {
        throw new ServiceLayerException('Invite code has been used up');
      }

      let user = await this.userRepository.findOne({
        email: dto.email,
      });
      if (user) {
        throw new ServiceLayerException('Email already registered');
      }

      user = this.userRepository.create(new User(dto.email, inviteCode));

      inviteCode.used++;

      await em.flush();

      return user;
    });

    return map({ from: user, to: UserResponseDto });
  }

  async isInviteCodeValid(code: string) {
    const inviteCode = await this.inviteCodeRepository.findOne({
      code,
    });
    return !!inviteCode && inviteCode.used < inviteCode.maxUses;
  }

  async userExists(email: string) {
    const user = await this.userRepository.findOne({
      email: email,
    });
    return !!user;
  }

  async createInviteCode(dto: CreateInviteCodeDto) {
    // a user is created just for testing purposes
    // usually the authenticated user would be injected into this service
    const adminUser = await this.userRepository.upsert(
      new User('admin@test.com'),
    );

    const inviteCode = this.inviteCodeRepository.create(
      new InviteCode(generateUserFriendlyId(), dto.maxUses, adminUser),
    );
    await this.inviteCodeRepository.getEntityManager().flush();
    return inviteCode.code;
  }
}

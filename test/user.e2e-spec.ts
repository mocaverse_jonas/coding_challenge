import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { UserModule } from '../src/user/user.module';
import * as request from 'supertest';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import config from '../src/mikro-orm.config';
import { App } from 'supertest/types';
import { nanoid } from 'nanoid';

describe('UserController (e2e)', () => {
  let app: INestApplication;
  let server: App;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [MikroOrmModule.forRoot(config), UserModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    server = await app.getHttpServer().listen(0);
  });

  afterAll(async () => {
    await app.close();
  });

  const MAX_USES = 10;
  const MAX_ATTEMPTS = MAX_USES * 2;
  it('Create User Race Condition Test', async () => {
    // call POST /code to generate invite code
    const { body } = await request(server)
      .post('/code')
      .send({ maxUses: MAX_USES });
    const inviteCode = body.inviteCode;

    // call GET /verifyCode to validate created invite code
    await request(server)
      .get(`/verifyCode?inviteCode=${inviteCode}`)
      .expect(200)
      .expect('Content-Type', /json/)
      .expect({ isValid: true });

    const userPrefix = `user${nanoid()}_`;
    // create MAX_ATTEMPTS parallel POST /user requests to simulate more
    // concurrent requests with the same invite code than available uses
    const requests = Array.from(Array(MAX_ATTEMPTS).keys()).map((i) =>
      request(server)
        .post('/user')
        .send({ inviteCode, email: `${userPrefix}${i + 1}@test.com` }),
    );

    const responses = await Promise.all(requests);

    // count the number of successful and unsuccessful requests
    const successfulRequests = responses.filter(
      (res) => res.status === 201,
    ).length;
    const unsuccessfulRequests = responses.filter(
      (res) => res.status === 400,
    ).length;

    // validate that MAX_USES requests returned status 201 and the rest returned status 400
    expect(successfulRequests).toBe(MAX_USES);
    expect(unsuccessfulRequests).toBe(MAX_ATTEMPTS - MAX_USES);
  });
});
